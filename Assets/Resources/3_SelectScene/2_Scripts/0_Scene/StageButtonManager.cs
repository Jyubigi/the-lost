﻿using UnityEngine;
using System.Collections;

public class StageButtonManager : MonoBehaviour {
    public const int stageNum = 5;
    public GameObject buttonOb = null;

    void OnEnable()
    {
        float bspace = 780/stageNum;
        for (int i = 0; i < stageNum; i++)
        {
            GameObject buttonKid = (GameObject)Instantiate(buttonOb);
            buttonKid.transform.parent = GameObject.Find("CC_Offset").transform;
            //0,0을 중심으로한 타일 뿌리기
            buttonKid.transform.localPosition = new Vector3(bspace * stageNum, bspace * stageNum, 1.0f);
            buttonKid.transform.localScale = Vector3.one;
        }
    }
    // Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}