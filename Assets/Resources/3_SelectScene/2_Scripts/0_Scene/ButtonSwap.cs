﻿using UnityEngine;
using System.Collections;

public class ButtonSwap : MonoBehaviour {
    public bool active = true;
    Vector3 _orimou = Vector3.zero;
    bool _isMoued = false;
    float _vcon = 0.0f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (active)
        {
            if (Input.GetMouseButtonDown(0))
            {
                _orimou = Input.mousePosition;
                _isMoued = true;
            }
            if (Input.GetMouseButtonUp(0))
            {
                _isMoued = false;
            }
            if (_isMoued)
            {
                //Debug.Log("mouse position " + Input.mousePosition.ToString());
                transform.localPosition = new Vector3(
                    Mathf.Clamp(transform.localPosition.x + _vcon * Time.deltaTime * 3.0f, -640.0f, 0.0f),
                        transform.localPosition.y,
                        0);
                Vector3 moubuf = Input.mousePosition;
                _vcon = moubuf.x - _orimou.x;// 단위시간당 x좌표 변화량
            }
            else
            {
                //Debug.Log("vcon" + _vcon.ToString());
                //Debug.Log("dt" + Time.deltaTime.ToString());
                if (transform.localPosition.x < 0.0f
                    && transform.localPosition.x > -640.0f)
                {
                    transform.localPosition = new Vector3(
                        transform.localPosition.x + _vcon * Time.deltaTime * 2.0f,
                        transform.localPosition.y,
                        1);
                    ;
                }
            }
        }
        
	}
}
