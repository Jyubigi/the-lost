﻿using UnityEngine;
using System.Collections;

public class Stage2Button : MonoBehaviour {
    public GameObject MapSelect = null;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        
	}
    public void Press()
    {
        if (MapSelect.transform.active == true)
        {
            MapSelect.SetActive(false);
            transform.parent.parent.GetComponent<ButtonSwap>().active = true;
        }
        else
        {
            MapSelect.SetActive(true);
            transform.parent.parent.GetComponent<ButtonSwap>().active = false;
        }
        
        PlayInfo.stageNum = 2;
        PlayInfo.mapNum = 1;
        MapSelect.transform.FindChild("StageLabel").GetComponent<UILabel>().text = "Stage 2";
    }
}
