﻿using UnityEngine;
using System.Collections;

public class StartButton : MonoBehaviour {
    MainBGM BGM = null;
    public CFadeOut Fader = null;
	// Use this for initialization
	void Start () {
        Fader = GameObject.Find("Fader").GetComponent<CFadeOut>();
        BGM = GameObject.Find("MainBGM").transform.GetComponent<MainBGM>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    public void Press()
    {
        Fader.startNextScene(5);
        BGM.isFade = true;
    }
}
