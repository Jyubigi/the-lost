﻿using UnityEngine;
using System.Collections;

public class ButtonLeft : MonoBehaviour {
     public UILabel NumLabel = null;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    public void Buttoned()
    {
        int mnum = PlayInfo.mapNum;
        --mnum;
        if (mnum < 1)
        {
            mnum = 1;
        }
        PlayInfo.mapNum = mnum;
        NumLabel.text = mnum.ToString();
    }
}
