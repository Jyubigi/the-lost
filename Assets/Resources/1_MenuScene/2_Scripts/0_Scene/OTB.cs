﻿using UnityEngine;
using System.Collections;

public class OTB : MonoBehaviour
{
    public GameObject spawnerOne = null;
	// Use this for initialization
	void Start ()
    {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
	
	}

    public void Release()
    {
        GameObject spChild = (GameObject)Instantiate(spawnerOne);
        spChild.transform.parent = GameObject.Find("UIRoot").transform;
        spChild.transform.localPosition = transform.localPosition;
    }
}
