﻿using UnityEngine;
using System.Collections;
using System;

public class EnterLoadPage : MonoBehaviour {
    public UILabel uitext = null;
    AsyncOperation async;
    bool isLoadGame;
    const float MAX_TIME = 1.5f;
    float _time = 0f;
    public IEnumerator StartLoad()
    {   
        if (!isLoadGame)
        {
            Debug.Log("Is Game Load");
            isLoadGame = true;
            async = Application.LoadLevelAsync(2);
            while (!async.isDone)
            {
                float p = async.progress * 100f;
                uitext.text =Convert.ToInt32(p).ToString() + "%";
                Debug.Log(p.ToString() + "%");
                yield return true;
            }
        }

    }
    // Use this for initialization
    void Start()
    {
        
    }
    float fTime = 0.0f;
    // Update is called once per frame
    void Update()
    {
        _time += Time.deltaTime;
        if (MAX_TIME < _time && _time<100f)
        {
            StartCoroutine("StartLoad");
            _time = 100.0f;
        }
    }
}
