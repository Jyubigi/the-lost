﻿using UnityEngine;
using System.Collections;

public class Outvome : MonoBehaviour {
    MainBGM BGM = null;
	// Use this for initialization
	void Start () {
        BGM = GameObject.Find("Game" + PlayInfo.stageNum.ToString()).transform.GetComponent<MainBGM>();
        BGM.isFade =  true;
	}
	
	// Update is called once per frame
	void Update () {
	}

    public void Lose()
    {
        PlayInfo.isPlaying = false;
        transform.FindChild("PpyLose").gameObject.SetActive(true);
        transform.FindChild("Ppyark").gameObject.SetActive(false);
        transform.FindChild("Scorelabel").GetComponent<UILabel>().text = " ";
    }
    public void Win(int Score)
    {
        PlayInfo.isPlaying = false;
        transform.FindChild("Ppyark").gameObject.SetActive(true);
        transform.FindChild("PpyLose").gameObject.SetActive(false);
        transform.FindChild("Ppyark").FindChild("PpyCrown").gameObject.SetActive(true);
        transform.FindChild("Scorelabel").GetComponent<UILabel>().text = Score.ToString();
    }
}
