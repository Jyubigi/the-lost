﻿using UnityEngine;
using System.Collections;

public class ArriveCompass : MonoBehaviour {
    public Transform ch = null;
    Vector3 arrivepos = Vector3.zero;
	// Use this for initialization
	void Start () {
        arrivepos =PlayInfo.arrivePosition;
    }
	
	// Update is called once per frame
	void Update () {
        float dx = arrivepos.x - ch.localPosition.x;
        float dy = arrivepos.y - ch.localPosition.y;

        float rad = Mathf.Atan2(dx, dy);
        float degree = rad * Mathf.Rad2Deg;
        transform.eulerAngles = new Vector3(0f, 0f, -degree + 180f);
	}
}
