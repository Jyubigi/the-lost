﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
public class MapLoad : MonoBehaviour {//맵 데이터를 읽어옵니다,
    
    void Awake()
    {
        PlayInfo.mapData = "";
        PlayInfo.monsterPosition.Clear();
        FileInfo theSourceFile = new FileInfo("map"+ PlayInfo.stageNum.ToString() + PlayInfo.mapNum.ToString() + ".txt");
        StreamReader reader = theSourceFile.OpenText();
        
        string readbuf = " ";
        readbuf = reader.ReadLine();
        Debug.Log(readbuf);
        int k = 0;
        string xx = "";
        string yy = "";
        //맵 크기 불러오기
        while (readbuf[k] != '*')
        {
            xx += readbuf[k];
            k++;
        }
        k++;
        while (k < readbuf.Length)
        {
            yy += readbuf[k];
            k++;
        }
        int mapWide = Int32.Parse(xx);
        int mapHight = Int32.Parse(yy);
        int tileSize = PlayInfo.tileSize;
        PlayInfo.mapWide = mapWide;
        PlayInfo.mapHight = mapHight;
        // 시작지점 불러오기
        readbuf = reader.ReadLine();
        k = 0;
        xx = "";
        yy = "";
        while (readbuf[k] != 's')
        {
            xx += readbuf[k];
            k++;
        }
        k++;
        while (k < readbuf.Length)
        {
            yy += readbuf[k];
            k++;
        }
        //시작지점에 플레이어 위치시키기
        PlayInfo.startPosition = new Vector3((float)(Int32.Parse(xx) * tileSize) + tileSize * 0.5f, (float)((mapHight - Int32.Parse(yy)) * tileSize) - tileSize * 0.5f, 1.00f);
        Debug.Log("start" + PlayInfo.startPosition);
        // 목적지점 불러오기
        readbuf = reader.ReadLine();
        k = 0;
        xx = "";
        yy = "";
        while (readbuf[k] != 'a')
        {
            xx += readbuf[k];
            k++;
        }
        k++;
        while (k < readbuf.Length)
        {
            yy += readbuf[k];
            k++;
        }
        //목적지점에 목적지 오브젝트 생성
        PlayInfo.arrivePosition = new Vector3((float)(Int32.Parse(xx) * tileSize) + tileSize * 0.5f, (float)((mapHight - Int32.Parse(yy)) * tileSize) - tileSize * 0.5f, 1.00f);
        Debug.Log("arrive" + PlayInfo.arrivePosition);
        readbuf = reader.ReadLine();
        int m = 0;
        // 몬스터 정보 불러오기
        while (readbuf[0] == 'm')
        {
            m = 1;
            xx = "";
            yy = "";
            while (readbuf[m] != 'm')
            {
                xx += readbuf[m];
                m++;
            }
            m++;
            while (m < readbuf.Length)
            {
                yy += readbuf[m];
                m++;
            }
            readbuf = reader.ReadLine();
            PlayInfo.monsterPosition.Add(new Vector3((float)(Int32.Parse(xx) * tileSize) + tileSize * 0.5f, (float)((mapHight - Int32.Parse(yy)) * tileSize) - tileSize * 0.5f, 1.00f));
        }
        m = 0;
        // 몬스터 정보 불러오기
        while (readbuf[0] == 't')
        {
            m = 1;
            xx = "";
            yy = "";
            while (readbuf[m] != 't')
            {
                xx += readbuf[m];
                m++;
            }
            m++;
            while (m < readbuf.Length)
            {
                yy += readbuf[m];
                m++;
            }
            readbuf = reader.ReadLine();
            PlayInfo.retimePosition.Add(new Vector3((float)(Int32.Parse(xx) * tileSize) + tileSize * 0.5f, (float)((mapHight - Int32.Parse(yy)) * tileSize) - tileSize * 0.5f, 1.00f));
        }
        // 타일맵 불러오기
        while (readbuf != null)
        {
            PlayInfo.mapData += readbuf;
            readbuf = reader.ReadLine();
        }
        reader.Close();

        //불러오기를 끝냅니다.
        PlayInfo.isPlaying = true;
    }
	void Start () {
	}
	// Update is called once per frame
	void Update () {
	
	}
}