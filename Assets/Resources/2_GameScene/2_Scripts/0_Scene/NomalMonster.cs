﻿using UnityEngine;
using System.Collections;
using System;

public class NomalMonster : MonoBehaviour
{
    enum eMove
    {
        idle = 0, x = 1, y = 2
    }
    enum eDirec
    {
        up =0 ,left =1,down=2,right=3
    }
    public GameObject[] _ani = new GameObject[4];
    eDirec _eAniInd = eDirec.left;
    Vector3[] _direcVal = new Vector3[4];//up left down right
    bool _isMove = false;

    // 뒤쫒는 오브젝트
    bool isChase = false;
    Transform _chase = null;

    // 뒤쫒는 오브젝트 좌표값
    Vector3 _chPos = Vector3.zero;

    //맵 충돌처리
    string _mapData = "";
    int _mw, _mh, _ts, _hhs;
    float speed = 400f;

    // Use this for initialization
    
    void Start()
    {
        //시작 시 목적지를 정해줍니다.
        _mapData = PlayInfo.mapData;
        _mw = PlayInfo.mapWide;
        _mh = PlayInfo.mapHight;
        _ts = PlayInfo.tileSize;
        _hhs = 256;
        for (int i = 0; i < 4; i++)
        {
            _ani[i].GetComponent<UISprite>().depth = 4;
            _ani[i].SetActive(false);
        }
        _eAniInd = eDirec.left;
        _ani[(int)_eAniInd].SetActive(true);
        _direcVal[0] = Vector3.up;
        _direcVal[1] = Vector3.left;
        _direcVal[2] = Vector3.down;
        _direcVal[3] = Vector3.right;
        StartCoroutine("CheckDirec");
    }
    public IEnumerator CheckDirec()
    {
        while (!PlayInfo.gameOver)
        {
            yield return new WaitForSeconds(0.2f);
            _ani[(int)_eAniInd].SetActive(false);
            // aniInd 설정
            int direc = (int)_eAniInd;
            for (int i = 0; i < 4; i++)
            {
                
                Vector3 hoDirec = new Vector3(_direcVal[direc].y, _direcVal[direc].x, 0);
                Vector3 colli = transform.localPosition + _direcVal[direc] * (float)_hhs;

                int cx = (int)colli.x / _ts;
                int cy = _mh - (int)(colli.y) / _ts - 1;
                
                //Debug.Log("now index" + x.ToString() +"by"+ y.ToString());
                if (_mapData[_mw * cy + cx] == '0')
                {
                    _eAniInd = (eDirec)direc;
                    break;
                }
                direc++;
                if (direc == 4)
                {
                    direc = 0;
                }
            }
            _ani[(int)_eAniInd].SetActive(true);
        }

    }
    public void setChase(Transform cha)
    {
        if (cha != null)
        {
            _chase = cha;
            isChase = true;
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (PlayInfo.gameOver)
            return;
        transform.localPosition += _direcVal[(int)_eAniInd] * Time.deltaTime * speed;
        
    }
}