﻿using UnityEngine;
using System.Collections;

public class Timer : MonoBehaviour {
    bool _isSaved = false;
    public CPlayer Cha = null;
    public Transform Pug = null;
    // Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	}
    public void Press()
    {
        audio.Play();
        if (_isSaved)
        {
            _isSaved = false;
            Cha.Go();
            Pug.gameObject.SetActive(false);
        }
        else
        {
            _isSaved = true;
            Cha.SavePosition();
            Pug.gameObject.SetActive(true);
        }
    }
}   
