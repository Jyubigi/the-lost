﻿using UnityEngine;
using System.Collections;

public class SetPPYPosition : MonoBehaviour {
    public SetPPYPosition Other = null;
    public Transform PPy = null;
    public bool isPPy = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    public void Press()
    {
        if (!isPPy)
        {
            isPPy = true;
            Other.isPPy = false;
            PPy.parent = transform;
            PPy.localPosition = Vector3.zero;
            PPy.localScale = Vector3.one;
        }
    }
}
