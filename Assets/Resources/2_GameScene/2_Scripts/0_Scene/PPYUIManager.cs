﻿using UnityEngine;
using System.Collections;

public class PPYUIManager : MonoBehaviour {

    //플레이타임 계산
    float _playTime = 0.0f;
    const float MAX_PLAY_TIME = 120f;
    bool _isStartTime = false;

    public Transform PpyBar = null;
    public GameSlider Slider = null;
    public Transform OutCome = null;
    public Transform Stop = null;
    

    //옵션
    public GameObject Map = null;
    public Compass Comp = null;
    public GameObject GayBar = null;
    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (PlayInfo.gameOver)
            return;
        Slider.value = (MAX_PLAY_TIME - _playTime) / MAX_PLAY_TIME;
        _playTime += Time.deltaTime;
        if (_playTime > MAX_PLAY_TIME)
        {
            GameOver("Fail");
            PlayInfo.gameOver = true;
        }
    }
    public void GameOver(string tmessage)
    {
        Stop.gameObject.SetActive(false);
        PpyBar.gameObject.SetActive(false);
        OutCome.gameObject.SetActive(true);
        if (tmessage == "c")
        {
            OutCome.GetComponent<Outvome>().Win((int)(MAX_PLAY_TIME - _playTime));
            Debug.Log("Win");
        }
        else
        {
            OutCome.GetComponent<Outvome>().Lose();
        }
    }
    public void recoverTime(float percentage)
    {
        _playTime -= percentage* MAX_PLAY_TIME;
        if(_playTime < 0)
        {
            _playTime = 0;
        }
        Slider.value = (MAX_PLAY_TIME - _playTime) / MAX_PLAY_TIME;
        Debug.Log(_playTime.ToString());
        Debug.Log(Slider.value.ToString());
    }
    public void startTimeBar()
    {
        _isStartTime = true;
    }
}
