﻿using UnityEngine;
using System.Collections;

public class PpyCrown : MonoBehaviour {
    float speed = 0.0f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (transform.active && transform.localPosition.y > 120.0f)
        {
            speed += Time.deltaTime * 5f;
            transform.localPosition += Vector3.down * speed;
        }
	}
}
