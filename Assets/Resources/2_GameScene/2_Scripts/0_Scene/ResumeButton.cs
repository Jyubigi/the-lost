﻿using UnityEngine;
using System.Collections;

public class ResumeButton : MonoBehaviour {

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
    }
    public void Release()
    {
        Time.timeScale = 1;
        if (PlayInfo.mapNum < 4)
        {
            ++PlayInfo.mapNum;
        }
        Application.LoadLevelAsync(5);
    }

}