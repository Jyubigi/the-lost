﻿using UnityEngine;
using System.Collections;

public class Pug : MonoBehaviour {
    public bool isFront  = true;
    public bool isRize = true;
    UISprite sprite = null;
	// Use this for initialization
	void Start () {
        sprite = transform.GetComponent<UISprite>();
        sprite.alpha = 0.0f;
	}
    public void setAlpha(float al)
    {
        sprite.alpha = al;
    }
    public float getAlpha()
    {
        return sprite.alpha;
    }
	// Update is called once per frame
	void Update () {
        if (isFront)
        {        }
        else
        {
            if (isRize)
            {
                sprite.alpha += Time.deltaTime *3;
                if (sprite.alpha >= 1f)
                {
                    isRize = false;
                }
            }
            else
            {
                sprite.alpha -= Time.deltaTime * 0.3f;
            }
        }
	}
}