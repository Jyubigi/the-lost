﻿using UnityEngine;
using System.Collections;

public class PPYStopButton : MonoBehaviour {
    public Transform OWindow = null;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	}
    public void Press()
    {
        if (OWindow.gameObject.active)
        {
            OWindow.gameObject.SetActive(false);
            Time.timeScale = 1f;
        }
        else
        {
            OWindow.gameObject.SetActive(true);
            Time.timeScale = 0f;
        }
    }
}
