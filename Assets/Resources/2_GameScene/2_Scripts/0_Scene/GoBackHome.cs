﻿using UnityEngine;
using System.Collections;

public class GoBackHome : MonoBehaviour {
    public CFadeOut fade = null;
    public int homenum = 1;
	// Use this for initialization
	void Start () {
        fade = GameObject.Find("Fader").GetComponent<CFadeOut>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    public void Press()
    {
        Time.timeScale = 1f;
        Debug.Log("PressHOme");
        fade.startNextScene(homenum);
    }
}