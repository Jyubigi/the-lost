﻿using UnityEngine;
using System.Collections.Generic;

public class PaintMapObject : MonoBehaviour
{// 스프라이트를 받아와서 맵에 뿌립니다.
    public List<GameObject> tileSprite;
    public GameObject arrivePoint = null;
    public Transform playerTr = null;
    public GameObject monsterOb = null;
    public GameObject retimeOb = null;
	// Use this for initialization
    void Start()
    {
        Transform originTra = GameObject.Find("TileOb").transform;
        Transform playerTr = GameObject.Find("character 1 1").transform;
        int mapHight = PlayInfo.mapHight;
        int mapWide = PlayInfo.mapWide;
        int tileSize = PlayInfo.tileSize;
        for (int i = mapHight - 1; i >= 0; i--)
        {
            for (int j = 0; j < mapWide; j++)
            {
                char wallc = PlayInfo.mapData.ToCharArray()[i * mapWide + j];
                if (wallc - 48 == 0)
                {
                    continue;
                }
                GameObject wallKid = (GameObject)(GameObject.Instantiate(tileSprite[wallc - 48]));
                wallKid.transform.parent = originTra;
                //0,0을 중심으로한 타일 뿌리기
                wallKid.transform.localPosition = new Vector3((float)(j * tileSize) + tileSize * 0.5f, (float)((mapHight - i) * tileSize) - tileSize * 0.5f, 1.00f);
                wallKid.transform.localScale = Vector3.one;
                wallKid.transform.GetComponent<UISprite>().depth = PlayInfo.stageNum * 2;
            }
        }
        GameObject arrive = (GameObject)GameObject.Instantiate(arrivePoint);
        arrive.transform.parent = originTra;
        arrive.transform.localScale = Vector3.one;
        arrive.transform.localPosition = PlayInfo.arrivePosition;
        arrive.transform.GetComponent<UISprite>().depth = 1;

        playerTr.transform.localScale = Vector3.one;
        playerTr.transform.localPosition = PlayInfo.startPosition;

        for (int k = 0; k < PlayInfo.monsterPosition.Count; k++)
        {
            GameObject monki = (GameObject)GameObject.Instantiate(monsterOb);
            monki.transform.parent = originTra;
            monki.transform.localScale = Vector3.one;
            monki.transform.localPosition = PlayInfo.monsterPosition[k];
        }
        for (int k = 0; k < PlayInfo.retimePosition.Count; k++)
        {
            GameObject monki = (GameObject)GameObject.Instantiate(retimeOb);
            monki.transform.parent = originTra;
            monki.transform.localScale = Vector3.one;
            monki.transform.localPosition = PlayInfo.retimePosition[k];
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
