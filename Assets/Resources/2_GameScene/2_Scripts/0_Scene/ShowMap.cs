﻿using UnityEngine;
using System.Collections;

public class ShowMap : MonoBehaviour {
    UISprite _sprite = null;
    bool isMapDown = false;
    bool isMapUp = false;
	// Use this for initialization
	void Start () {
        _sprite = transform.parent.GetComponent<UISprite>();
        transform.GetComponent<UISprite>().spriteName = PlayInfo.stageNum.ToString() +  PlayInfo.mapNum.ToString();
	}
    public void ButtonPress()
    {
        if (PlayInfo.gameOver)
            return;
        audio.Play();
        if (isMapDown == true)
        {
            _sprite.spriteName = "map2";
            isMapDown = false;
            isMapUp = true;
        }
        else if (isMapUp == true)
        {
            _sprite.spriteName = "map";
            isMapDown = true;
            isMapUp = false;
        }
        else
        {
            _sprite.spriteName = "map";
            isMapDown = true;
        }
    }
	// Update is called once per frame
	void Update () {
        if (isMapUp)
        {
            if (transform.localPosition.y <= 530)
            {
                transform.localPosition += Vector3.up * 100;
            }
        }
        if (isMapDown)
        {
            if (transform.localPosition.y > -400)
            {
                transform.localPosition += Vector3.down * 100;
            }
        }
	}
}
