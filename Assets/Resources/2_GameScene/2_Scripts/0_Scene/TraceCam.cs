﻿using UnityEngine;
using System.Collections;

public class TraceCam : MonoBehaviour 
{
    public float smoothTime = 1f;
    public Transform target = null;
    float _tick = 0f;

    // 카메라 무빙 관련
    public enum CamState
    {
        Null,
        Zoomin,
        Zoomout
    }

    [HideInInspector]
    CamState camState = CamState.Null;
    [HideInInspector]
    public float zoomTick = 0f;
    float _smoothZoom =1.7f;

    public void setCamState(CamState cs)
    {
        zoomTick = 0.0f;
        camState = cs;
    }
    void LateUpdate()
    {
        Vector3 camPos = this.transform.position;

        if (_tick <= 1)
        {
            _tick += Time.deltaTime/10f;
            camPos = Vector3.Lerp(this.transform.position, target.position, _tick);
        }
        else
            _tick = 0f;

        this.transform.position = camPos - new Vector3(0f, 0f, 0f);

        // 카메라 줌 관련
        if (camState.Equals(CamState.Zoomin))
        {
            if (zoomTick <= 1f)
            {
                zoomTick += Time.deltaTime * (4f * 0.1f);
                _smoothZoom = Mathf.Lerp(_smoothZoom, 1.2f, zoomTick);
            }
        }
        else if (camState.Equals(CamState.Zoomout))
        {
            if (zoomTick <= 1f)
            {
                zoomTick += Time.deltaTime * (4f * 0.1f);
                _smoothZoom = Mathf.Lerp(_smoothZoom, 1.5f, zoomTick);
            }
        }
        else
            _smoothZoom = 1.7f;

        transform.camera.orthographicSize = _smoothZoom;
        UIInfo.cameraSize = _smoothZoom;
    }
}
