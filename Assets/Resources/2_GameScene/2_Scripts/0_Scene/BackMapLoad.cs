﻿using UnityEngine;
using System.Collections;
using System;

public class BackMapLoad : MonoBehaviour {// 백그라운드 스프라이트를 뿌려줍니다.
    public GameObject backSprite = null;
	// Use this for initialization
    void Start()
    {
        Debug.Log("backLoad");
        Transform originTra = GameObject.Find("TileOb").transform;
        int mapWide = PlayInfo.mapWide;
        int mapHight = PlayInfo.mapHight;
        int tileSize = PlayInfo.tileSize;

        for (int i = mapHight - 1; i >= 0; i--)
        {
            for (int j = 0; j < mapWide; j++)
            {
                GameObject wallKid = (GameObject)(GameObject.Instantiate(backSprite));
                wallKid.transform.parent = originTra;
                wallKid.transform.localPosition = new Vector3((float)(j * tileSize) + tileSize * 0.5f, (float)((mapHight - i) * tileSize) - tileSize * 0.5f, 1.00f);
                wallKid.transform.localScale = Vector3.one;
                wallKid.transform.GetComponent<UISprite>().depth = 0;
            }
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
