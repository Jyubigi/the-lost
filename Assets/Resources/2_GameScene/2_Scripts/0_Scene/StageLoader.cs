﻿using UnityEngine;
using System.Collections;

public class StageLoader : MonoBehaviour {
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Awake () {
        Debug.Log("StageLoad..............");
        string buffer = "2_GameScene/1_Prefabs/Game" + PlayInfo.stageNum.ToString();
        GameObject stage  =(GameObject)GameObject.Instantiate(Resources.Load(buffer));
        stage.transform.parent = transform;
        stage.transform.localScale = Vector3.one;
	}
}