﻿using UnityEngine;
using System.Collections;

public class GameSlider : MonoBehaviour {
    public Transform Ppyak = null;
    public Transform escape = null;
    public float value = 1.0f;
	// Use this for initializations
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        transform.localScale
            = new Vector3(value, 1f, 1f);
        Ppyak.transform.localPosition =
            new Vector3(600f*value - 300f,Ppyak.transform.localPosition.y,1f);
        if (escape != null && value < 0.5f)
        {
            escape.active = true;
        }
	}
}
