﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;

public static class PlayInfo {
    public static bool isPlaying = false;
    public static int mapNum = 4;
    public static int mapWide = 11;
    public static int mapHight = 11;
    public static int tileSize = 512;
    public static bool gameOver = false;
    public static int stageNum = 2;
    public static string mapData = "";
    public static Vector3 startPosition = Vector3.zero;
    public static Vector3 arrivePosition = Vector3.zero;
    public static List<Vector3> monsterPosition = new List<Vector3>();
    public static List<Vector3> retimePosition = new List<Vector3>();
}// 맵 번호를 넘겨줍니다__