﻿using UnityEngine;
using System.Collections;

public class PpyAni : MonoBehaviour {
    float _timer = 0.0f;
    float _speed = 9.0f;
    bool isDown = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (!isDown)
        {
            transform.localPosition += Vector3.up * _speed;
            _timer += Time.deltaTime;
            if (_timer >= 0.2f)
            {
                isDown = true;
                _timer = 0.0f;
            }
        }
        else
        {
            if (transform.localPosition.y > 300f)
            {
                transform.localPosition += Vector3.down * _speed * _timer;
                _timer += Time.deltaTime;
            }
            
        }
	}
}
