﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CPlayer : MonoBehaviour
{
    // 충돌범위 발자국 스프라이트 위치에 맞게 조정
    // 스탠딩 애니메이션
    public enum PlayerState { idle, move, standAni };
    PlayerState _eState = PlayerState.standAni;
    // 발자국 정보
    Vector3 TURN_DEGREE = new Vector3(0f,0f,15f);
    float _mp = -1f;//+-값
    const float STEP_DISTAN = 160f;
    const float FOOT_DISTAN = 30.0f;//왼발과 오른발 간격
    Vector3 _stepPosition = Vector3.zero;
    Vector3 _nextPos = Vector3.zero;
    float _stepTime = 0f;
    const float STEP_TIME = 0.2f;

    // standAni
    const float FIRST_STEP_TIME = 0.4f;
    const float SECOND_STEP_TIME = 1.0f;
    float _standTime = 0.0f;
    int _standCount = 0;
    Vector3 _standAngle = Vector3.zero;
    Vector3 _standPosition = Vector3.zero;

    public PPYUIManager  _oui = null;
    //////////////////////////////////////
    //Vector3 _direcVec = 

    //이동관련 = 발자국
    public JpyStick _joyStick = null;
    Vector3 _direcVal = Vector3.zero;
    Vector3[] _hoDirecVal = new Vector3[2];
    int _hoInd = 0;//x,y
    public float speed = 50.0f;
    
    float _movetime = 0f;

    bool _isStandingAni = false;

    // 좌표저장
    Vector3 _backPos = PlayInfo.startPosition;
    //카메라
    public Transform _camera = null;

    //맵 충돌처리
    string mapData = "";
    int _mw, _mh, _ts, _ths, _tws;

    // 발자국 남기기
    public GameObject pug = null;//발자국 원본

    List<GameObject> _pugs = new List<GameObject>(); // 화면에 찍히는 발자국 오브젝트

    const int _pugNum = 15;
    int _frontInd = 0;
    int _rearInd = _pugNum -1;
    bool _isPugStop = true;
    
    //r그리드
    float _gx= 0.0f, _gy = 0.0f;
    bool _isGriding = false;
    Transform _pBB = null;

    void OnTriggerEnter(Collider other)
    {
        if(PlayInfo.gameOver)
            return;
        Debug.Log("PlayTrigger");
        if (other.gameObject.tag.Equals("Monster"))
        {
            PlayInfo.gameOver = true;
            _oui.GameOver("Fail");
            Debug.Log("GAME OVER");
            StopPugOne();
            StopPugTwo();
            _eState = PlayerState.idle;
        }
        if (other.gameObject.tag.Equals("Arrive"))
        {
            PlayInfo.gameOver = true;
            _oui.GameOver("c");
            StopPugOne();
            StopPugTwo();
            _eState = PlayerState.idle;
        }
        if (other.gameObject.tag.Equals("recovertime"))
        {
            other.gameObject.transform.audio.Play();
            _oui.recoverTime(1f);
            GameObject.Destroy(other.gameObject);
        }
    }
    public IEnumerator CheckPlayerState()
    {
        while (!PlayInfo.gameOver)
        {
            Debug.Log("setState");
            yield return new WaitForSeconds(0.2f);
            _direcVal = _joyStick.getPosToken();
            if (_direcVal == Vector3.zero)
            {
                if (_standCount >= 2)
                {
                    _eState = PlayerState.idle;
                }
                else
                {
                    _eState = PlayerState.standAni;
                }
            }
            else
            {
                _eState = PlayerState.move;
            }
        }

    }
    public IEnumerator ActionSelect()
    {
        while (!PlayInfo.gameOver)
        {
            switch (_eState)
            {
                case PlayerState.idle:
                    Idle();
                    break;
                case PlayerState.move:
                    Move();
                    break;
                case PlayerState.standAni:
                    StandAni();
                    break;
            }
            yield return null;
        }
    }
    void Start()
    {
        PlayInfo.gameOver = false;
        transform.GetComponent<AudioSource>().clip.name = "step" + PlayInfo.stageNum.ToString();
        
        mapData = PlayInfo.mapData;
        _mw = PlayInfo.mapWide;
        _mh = PlayInfo.mapHight;
        _ts = PlayInfo.tileSize;
        Debug.Log("mh" + PlayInfo.mapHight.ToString());
        _ths = 75;//충돌값 : 앞뒤
        _tws = 75;//충돌값 : 좌우

        _pBB = transform.FindChild("BB");
        for (int i = 0; i < _pugNum; i++)
        {
            GameObject pugkid = (GameObject)Instantiate(pug);
            pugkid.transform.parent = GameObject.Find("GameRoot").transform;
            pugkid.transform.localScale = Vector3.one;
            pugkid.transform.GetComponent<UISprite>().depth = 3;
            pugkid.GetComponent<Pug>().isFront = false;
            _pugs.Add(pugkid);
        }
        _rearInd = _pugNum - 1;
        _frontInd = 0;
        _pugs[_rearInd].GetComponent<Pug>().isFront = true;
        _pugs[_frontInd].GetComponent<Pug>().isFront = true;
        _direcVal = Vector3.left;
        _hoDirecVal[0] = new Vector3(_direcVal.y, -_direcVal.x, 0f);
        _hoDirecVal[1] = new Vector3(-_direcVal.y, _direcVal.x, 0f);
        _stepPosition = transform.localPosition;
        StartCoroutine("CheckPlayerState");
        StartCoroutine("ActionSelect");
    }
    public void Idle()
    {
    }
    public void StandAni()
    {
        _standTime += Time.deltaTime;
        if (_standCount ==1 && _standTime > SECOND_STEP_TIME)
        {
            _standCount++;
            StopPugTwo();
        }
        if (_standCount == 0 && _standTime > FIRST_STEP_TIME)
        {
            _standCount++;
            StopPugOne();
        }
    }
    public void Move()
    {
        if (_standCount !=0)
        {
            _standCount = 0;
            _standTime = 0f;
            StartPug();
        }
        if (isCorrectStampPosition())
        {
            transform.localPosition += _direcVal * speed * Time.deltaTime;
            if (getStepDistan() > STEP_DISTAN)
            {
                StampPug();
            }
            
        }
    }
    

    public void SavePosition()
    {
        _backPos = transform.localPosition;
    }
    public void Go()
    {
        transform.localPosition = _backPos;
    }
    // Use this for initialization
    float getStepDistan()
    {
        return Mathf.Sqrt((transform.localPosition.x - _stepPosition.x)
            * (transform.localPosition.x - _stepPosition.x)
            + (transform.localPosition.y - _stepPosition.y)
            * (transform.localPosition.y - _stepPosition.y));
    }
        
    void StartPug()
    {
        _isStandingAni = false;
        for (int i = 0; i < _pugs.Count; i++)
        {
            _pugs[i].GetComponent<Pug>().isFront = false;
        }
        _pugs[_rearInd].GetComponent<Pug>();
    }
    bool isCorrectStampPosition()
    {
        _hoDirecVal[0] = new Vector3(_direcVal.y, -_direcVal.x, 0f);
        _hoDirecVal[1] = new Vector3(-_direcVal.y, _direcVal.x, 0f);
        //여기부터 충돌처리
        int xp = (int)_direcVal.x;
        int yp = (int)_direcVal.y * -1;
        _nextPos = transform.localPosition + _hoDirecVal[_hoInd] * FOOT_DISTAN * Random.Range(0.2f, 1.5f);
        //원 충돌처리
        Vector3 colliMin = _nextPos + _direcVal * (float)_ths + _hoDirecVal[0] * (float)_tws;
        Vector3 colliMax = _nextPos + _direcVal * (float)_ths + _hoDirecVal[1] * (float)_tws;
        int minx = (int)colliMin.x / _ts;
        int miny = _mh - (int)(colliMin.y) / _ts - 1;
        int maxx = (int)colliMax.x / _ts;
        int maxy = _mh - (int)(colliMax.y) / _ts - 1;

        if (mapData[_mw * miny + minx] != '0'
            || mapData[_mw * maxy + maxx] != '0')
        {
            return false;
        }
        return true;
    }
    void StampPug() // 발자국 찍기
    {
        audio.Play();
        _pugs[_rearInd].GetComponent<Pug>().isFront = true;
        _pugs[_rearInd].GetComponent<Pug>().isRize = true;
        _pugs[_rearInd].GetComponent<Pug>().setAlpha(0.5f);
        //_pugs[_rearInd].GetComponent<Pug>().AlphaReset();
        _stepPosition = _nextPos;
        _pugs[_rearInd].transform.localPosition = _nextPos;
        _hoInd = 1 - _hoInd;
        _pugs[_rearInd].transform.eulerAngles = _joyStick.getStickAngle() + _mp * TURN_DEGREE;
        _mp *= -1f;
        // 이전 머리발자국 프론트 해제
        _pugs[_frontInd].GetComponent<Pug>().isFront = false;
        // 인덱스 떙기기
        _frontInd = _rearInd;
        --_rearInd;
        if (_rearInd <0)
        {
            _rearInd = _pugNum -1;
        }
    }
    void StopPugOne()
    {
        audio.Play();
        for (int i = 0; i < _pugs.Count; i++)
        {
            _pugs[i].GetComponent<Pug>().isFront = true;
            _pugs[i].GetComponent<Pug>().setAlpha(_pugs[i].GetComponent<Pug>().getAlpha() * 0.8f);
            _pugs[i].GetComponent<Pug>().isRize = false;
        }
        _standPosition =transform.localPosition;
        _standAngle = _pugs[_frontInd].transform.localRotation.eulerAngles;

        _pugs[_rearInd].GetComponent<Pug>().setAlpha(1f);
        _pugs[_rearInd].transform.localPosition =
            _standPosition + _direcVal * _ths / 2 + _hoDirecVal[_hoInd] * FOOT_DISTAN *2;
        _hoInd = 1 - _hoInd;
        //Debug.Log(_hoDirecVal.ToString());
        _pugs[_rearInd].transform.eulerAngles = _standAngle + TURN_DEGREE * _mp;
        _mp *= -1;
        _frontInd = _rearInd;
        --_rearInd;
        if (_rearInd < 0)
        {
            _rearInd = _pugNum - 1;
        }
    }
    void StopPugTwo()
    {
        audio.Play();
        _pugs[_rearInd].GetComponent<Pug>().setAlpha(1f);
        _pugs[_rearInd].transform.localPosition =
            _standPosition + _direcVal * _ths / 2 + _hoDirecVal[_hoInd] * FOOT_DISTAN *2;
        _hoInd = 1 - _hoInd;
        //Debug.Log(_hoDirecVal.ToString());
        _pugs[_rearInd].transform.eulerAngles = _standAngle + TURN_DEGREE * _mp;
        _mp *= -1;
        _frontInd = _rearInd;
        --_rearInd;
        if (_rearInd < 0)
        {
            _rearInd = _pugNum - 1;
        }
    }
    void Update()
    {
    }
}