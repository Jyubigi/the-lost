﻿using UnityEngine;
using System.Collections;

public class akakak : MonoBehaviour
{
    public GameObject spawnerOne = null;
    GameObject spChild = null;
    bool isAlive = false;
	// Use this for initialization
	void Start ()
    {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
	
	}
    public void Release()
    {
        if (isAlive)
        {
            isAlive = false;
            GameObject.Destroy(spChild);
        }
        else
        {
            isAlive = true;
            spChild = (GameObject)Instantiate(spawnerOne);
            spChild.transform.parent = GameObject.Find("PopupOffset").transform;
            spChild.transform.localPosition = transform.localPosition;
            spChild.transform.localScale = Vector3.one;
        }
        
    }
}
