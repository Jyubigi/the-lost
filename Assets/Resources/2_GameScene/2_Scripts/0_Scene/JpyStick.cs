﻿using UnityEngine;
using System.Collections;

public class JpyStick : MonoBehaviour {
    Vector3 padPos = Vector3.zero;
    bool _mouseDown = false;
    const float PAD_RAD = 100f;
    Vector3 _posToken = Vector3.zero;
    Vector3 mpos = Vector3.zero;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        if (PlayInfo.gameOver)
        {
            return;
        }
        mpos = Input.mousePosition;
        //각도계산
        float dx = mpos.x - padPos.x;
        float dy = mpos.y - padPos.y;
        float rad = -Mathf.Atan2(dx, dy) + Mathf.PI/2;
        float degree = rad * Mathf.Rad2Deg;// rad에서 180빼주면 될거같은데

        transform.eulerAngles = new Vector3(0f, 0f, degree + 90f);
        //움직일 값 계산
        _posToken = new Vector3(Mathf.Cos(rad) * PAD_RAD * 0.01f, Mathf.Sin(rad) * PAD_RAD * 0.01f, 0f);

        transform.localPosition = _posToken * 250f;
        if (Input.GetMouseButtonDown(0))
        {
            _mouseDown = true;
            padPos = Input.mousePosition;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            _mouseDown = false;
        }
	}
    public Vector3 getPosToken()
    {
        if (_mouseDown)
        {
            return _posToken;
        }
        else
        {
            return Vector3.zero;
        }
    }
    public Vector3 getStickAngle()
    {
        return transform.eulerAngles -Vector3.forward* 180f;
    }
}
