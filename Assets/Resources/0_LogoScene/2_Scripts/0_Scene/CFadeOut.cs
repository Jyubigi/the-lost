﻿using UnityEngine;
using System.Collections;

public class CFadeOut : MonoBehaviour {

    public bool _isStart = false;
    int _sceneLevel = 1;
    //bool _isLoadGame = false;

    private float fadeSpeed = 0.5f;
    private int drawDepth = -1000;
    public float time;
    private float alpha = 1.0f;
    public Texture2D FadeTexture; //씬이 넘어갈때 사용될 텍스쳐 그림
    public Texture2D LodingTexture; //로딩화면을 사용할때 사용할 텍스쳐
    void Start()
    {
        DontDestroyOnLoad(this);
        alpha = 0;
    }
    IEnumerator StartLoad()
    {
        AsyncOperation async = Application.LoadLevelAsync(_sceneLevel);
        while (!async.isDone)
        {
            yield return true;
        }

    }
    public void startNextScene(int scenelevel)
    {
        _sceneLevel = scenelevel;
        _isStart = true;
        time = 0;
        alpha = 0;
    }
    void OnGUI()
    {
        if (_isStart)
        {
            time += Time.deltaTime;		//타임저장
            alpha += fadeSpeed * Time.deltaTime; //색이 바뀌는 부분
            alpha = Mathf.Clamp01(alpha); 							//알파값 연산해서 변환
            GUI.skin = null;
            Color tmpcolor = new Color(GUI.color.r, GUI.color.g, GUI.color.b, alpha);

            GUI.color = tmpcolor;
            GUI.depth = drawDepth;
            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), FadeTexture); //텍스쳐를 받아 그리는 부분

            if (alpha == 0)//페이드 아웃	
            {
                GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), LodingTexture);
            }
            if (alpha == 1)//페이드가 끝나면 다음 씬으로 넘어감
            {
                StartCoroutine("StartLoad");
                transform.GetComponent<CFadeIn>().startScene();
                _isStart = false;
            }
        }
    }
}