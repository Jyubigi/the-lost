﻿using UnityEngine;
using System.Collections;

public class MainBGM : MonoBehaviour {
    public bool isFade = false;
    const float FADE_TIME = 1.0f;
    float fadeTime = 0.0f;
    AudioSource audios =null;

	// Use this for initialization
	void Start () {
        DontDestroyOnLoad(this);
        audios = transform.GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
        if (isFade)
        {
            fadeTime += Time.deltaTime;
            audios.volume = FADE_TIME - fadeTime;
            if (fadeTime > FADE_TIME)
            {
                fadeTime = 0f;
                isFade = false;
            }
        }
	}
}
